$(function() {
    var $window = $(window),
    $stickyEl = $('#toolbar'),
    elTop = $stickyEl.offset().top;

    $window.scroll(function() {
        $stickyEl.toggleClass('sticky', $window.scrollTop() > elTop);
    });

});
